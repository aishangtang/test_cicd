using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CICD.Demo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok("CICD测试程序V2022");
        }
    }
}
